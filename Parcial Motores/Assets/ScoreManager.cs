using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshPro puntaje;
    public int score;

    private void Start()
    {
        score = 0;
        UpdateScoreText();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Detector"))
        {
            score += 1000;
            UpdateScoreText();
        }
    }

    public void UpdateScoreText()
    {
        puntaje.text = "Score: " + score.ToString();
    }
}
