using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Vida : MonoBehaviour
{
    public TextMeshPro _vida;
    public int vida;
    Movimiento _movimiento;

    private void Start()
    {
        _movimiento = FindObjectOfType<Movimiento>();
        UpdateVida();
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }

    public void UpdateVida()
    {
        _vida.text = "Vida: " + _movimiento.vida.ToString();
    }
}
