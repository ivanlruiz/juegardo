using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo : MonoBehaviour
{
    public float velocidad = 5f;

    private void Update()
    {        
        transform.Translate(Vector3.left * velocidad * Time.deltaTime);
    }
}
