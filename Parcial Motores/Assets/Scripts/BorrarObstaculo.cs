using Unity.VisualScripting;
using UnityEngine;

public class BorrarObstaculo : MonoBehaviour
{
    private ScoreManager scoreManager;
    private GeneradorObstaculos generadorObstaculos;
    private Obstaculo obstaculos;
    private Movimiento _movimiento;
    private int obstaculo;

    private void Start()
    {
        _movimiento = FindObjectOfType<Movimiento>();
        scoreManager = FindObjectOfType<ScoreManager>();
        generadorObstaculos = FindObjectOfType<GeneradorObstaculos>();
        obstaculos = FindObjectOfType<Obstaculo>();
    }

    private void Update()
    { }

    private bool alreadyLostLife = false;

    private void OnTriggerEnter(Collider collision)
    {
        bool isPlayerTagCubo = this.gameObject.CompareTag("Cubo");
        bool isDetectorTagCubo = collision.gameObject.CompareTag("DetectorCubo");

        bool isPlayerTagCapsula = this.gameObject.CompareTag("Capsula");
        bool isDetectorTagCapsula = collision.gameObject.CompareTag("DetectorCapsula");

        bool isPlayerTagEsfera = this.gameObject.CompareTag("Esfera");
        bool isDetectorTagEsfera = collision.gameObject.CompareTag("DetectorEsfera");

        bool Detector = isPlayerTagCubo || isPlayerTagCapsula || isPlayerTagEsfera;

        if ((isPlayerTagCubo && isDetectorTagCubo) || (isPlayerTagCapsula && isDetectorTagCapsula) || (isPlayerTagEsfera && isDetectorTagEsfera))
        {
            Destroy(collision.gameObject);
            Transform padre = collision.gameObject.transform.parent;
            if (padre != null)
            {
                Destroy(padre.gameObject);
            }
            obstaculo += 1;
            scoreManager.score += 1;
            scoreManager.UpdateScoreText();
        }
        else if (Detector && !alreadyLostLife)
        {
            _movimiento.vida--;
            StartCoroutine(_movimiento.Blink());
            alreadyLostLife = true;
        }

        if (collision.gameObject.CompareTag("Obstaculo"))
        {
            _movimiento.vida--;
            StartCoroutine(_movimiento.Blink());
            alreadyLostLife = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        alreadyLostLife = false;
    }

}
