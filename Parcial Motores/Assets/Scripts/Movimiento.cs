using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public float velocidad = 10f;
    public float vida = 5f;
    private Renderer playerRenderer;
    Vida _vida;
    private void Start()
    {
        _vida = FindObjectOfType<Vida>();
        playerRenderer = GetComponent<Renderer>();
    }
    void Update()
    {
        float movimientoVertical = Input.GetAxis("Vertical");
        float velocidadMovimiento = velocidad * movimientoVertical;
        transform.Translate(Vector3.up * velocidadMovimiento * Time.deltaTime);

        if (vida <= 0)
        {
            Destroy(this);
        }
    }

    public IEnumerator Blink()
    {
        float blinkDuration = 1.0f;
        float blinkInterval = 0.1f;
        float elapsedTime = 0.0f;
        bool isVisible = true;

        while (elapsedTime < blinkDuration)
        {
            playerRenderer.enabled = isVisible;
            yield return new WaitForSeconds(blinkInterval);
            isVisible = !isVisible;
            elapsedTime += blinkInterval;
        }
        playerRenderer.enabled = true;
    }

}
