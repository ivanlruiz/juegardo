using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour
{
    public float fadeDuration = 1.0f; // Duraci�n del desvanecimiento en segundos
    public Image fadeImage; // Referencia al componente Image del objeto de imagen

    private float currentAlpha = 1.0f; // Alfa actual del desvanecimiento

    void Start()
    {
        fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 1.0f); // Establece el alfa inicial al m�ximo
    }

    void Update()
    {
        // Reduce gradualmente el alfa del desvanecimiento
        currentAlpha -= Time.deltaTime / fadeDuration;
        fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, currentAlpha);

        if (currentAlpha <= 0.0f)
        {
            // Desactiva el objeto de imagen cuando el desvanecimiento se haya completado
            gameObject.SetActive(false);
        }
    }
}
