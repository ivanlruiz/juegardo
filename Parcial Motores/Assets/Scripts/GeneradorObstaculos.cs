using UnityEngine;

public class GeneradorObstaculos : MonoBehaviour
{
    public GameObject[] obstaculos;
    public float[] alturasGeneracion;
    public float tiempoGeneracion = 1f;

    private float tiempoOriginal = 0f;
    private float tiempoSiguienteGeneracion;
    private float limiteX = -35f;
    private float limiteX2 = 25f;
    public int generado;

    BeatController _beatController;
    Intervals[] _intervals;

    void Start()
    {
        _beatController = FindObjectOfType<BeatController>();
        /*tiempoSiguienteGeneracion = Time.time + tiempoGeneracion;
        tiempoOriginal = tiempoGeneracion;*/
    }

    void Update()
    {
/*        
        if (Time.time >= tiempoSiguienteGeneracion)
 {
     
     Debug.Log(generado);
     Debug.Log(tiempoOriginal);
     Generador();
     tiempoSiguienteGeneracion = Time.time + tiempoGeneracion;

     if (generado == 20)
     {
         tiempoGeneracion += tiempoOriginal / 2;
     }

     if(generado == 32)
     {
         tiempoGeneracion = tiempoOriginal/2f;
     }
                                                                                              }
        */
        Limite();
    }
    public void Generador()
    {
        generado++;
        Debug.Log(generado);
        int indiceAleatorio = Random.Range(0, obstaculos.Length);
        GameObject obstaculoPrefab = obstaculos[indiceAleatorio];
        float alturaAleatoria = alturasGeneracion[Random.Range(0, alturasGeneracion.Length)];
        Vector3 posicion = new Vector3(transform.position.x, alturaAleatoria, transform.position.z);
        Instantiate(obstaculoPrefab, posicion, Quaternion.identity);    
    }
    void Limite()
    {
        GameObject[] obstaculosEnEscena = GameObject.FindGameObjectsWithTag("Obstaculo");
        foreach (GameObject obstaculo in obstaculosEnEscena)
        {
            if (obstaculo.transform.position.x < limiteX)
            {
                Destroy(obstaculo);
            }if (obstaculo.transform.position.x > limiteX2)
            {
                Destroy(obstaculo);
            }
        }
    }
}
