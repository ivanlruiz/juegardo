using System.Collections;
using UnityEngine;

public class Pulsos : MonoBehaviour
{
    [SerializeField] bool _useTestBeat;
    [SerializeField] float _pulseSize = 1.15f;
    [SerializeField] float _returnSpeed = 5;
    [SerializeField] float _heightDecreaseSpeed = 0.5f; // Velocidad de disminución de _Height
    [SerializeField] float _heightTarget = 0.5f; // Valor objetivo para _Height
    Vector3 _startSize;
    public Renderer _renderer;
    private Coroutine _heightCoroutine;

    private void Start()
    {
        _startSize = transform.localScale;
        if (_useTestBeat)
        {
            StartCoroutine(TestBeat());
        }
    }

    private void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, _startSize, Time.deltaTime * _returnSpeed);
    }

    public void Locura()
    {
        _renderer.material.SetFloat("_SineRepetitions", 1000);
        _renderer.material.SetFloat("_Speed", -10);
    }
    public void Sines()
    {
        
    }
    public void Height()
    {
        _renderer.material.SetFloat("_Height", .7f);

        if (_heightCoroutine != null)
        {
            StopCoroutine(_heightCoroutine); // Detener la disminución de _Height si está en curso
        }

        _heightCoroutine = StartCoroutine(DecreaseHeight());
    }

    public void Pulse()
    {
        transform.localScale = _startSize * _pulseSize;
    }

    IEnumerator DecreaseHeight()
    {
        float currentHeight = _renderer.material.GetFloat("_Height");

        while (currentHeight > _heightTarget)
        {
            currentHeight -= _heightDecreaseSpeed * Time.deltaTime;
            _renderer.material.SetFloat("_Height", currentHeight);
            yield return null;
        }

        _renderer.material.SetFloat("_Height", _heightTarget);
        _heightCoroutine = null;
    }

    IEnumerator TestBeat()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            Pulse();
        }
    }
}