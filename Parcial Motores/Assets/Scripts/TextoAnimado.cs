using UnityEngine;
using TMPro;

public class TextoAnimado : MonoBehaviour
{
    public TextMeshPro textMeshPro;
    public float startYPosition;
    public float endYPosition;
    public float animationDuration = 2f;

    private void Start()
    {
        textMeshPro = GetComponent<TextMeshPro>();
        startYPosition = transform.position.y + 10f; // Ajusta la posici�n inicial del texto seg�n tus necesidades
        endYPosition = transform.position.y;

        StartCoroutine(AnimateText());
    }

    private System.Collections.IEnumerator AnimateText()
    {
        float elapsedTime = 0f;
        Vector3 startPosition = new Vector3(transform.position.x, startYPosition, transform.position.z);
        Vector3 endPosition = new Vector3(transform.position.x, endYPosition, transform.position.z);

        while (elapsedTime < animationDuration)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, elapsedTime / animationDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(2f); // Espera 2 segundos

        elapsedTime = 0f;

        while (elapsedTime < animationDuration)
        {
            transform.position = Vector3.Lerp(endPosition, startPosition, elapsedTime / animationDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        Destroy(gameObject); // Elimina el objeto de texto cuando la animaci�n haya terminado
    }
}
