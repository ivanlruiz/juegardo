using UnityEngine;

public class Detector : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            string obstaculoNombre = transform.parent.name;

            switch (obstaculoNombre)
            {
                case "Cubo":
                    Destroy(transform.parent.gameObject);
                    break;
                case "Esfera":
                    Destroy(transform.parent.gameObject);
                    break;
                case "Capsula":
                    Destroy(transform.parent.gameObject);
                    break;
                default:
                    break;
            }
        }
    }
}
