using UnityEngine;

public class Forma : MonoBehaviour
{
    public GameObject esferaPrefab;
    public GameObject capsulaPrefab;
    public GameObject cuboPrefab;

    public Material jugadorMaterial;

    void Start()
    {
        MeshRenderer jugadorMeshRenderer = GetComponent<MeshRenderer>();
        jugadorMaterial = jugadorMeshRenderer.sharedMaterial;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            CambiarFormaObjeto(esferaPrefab);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            CambiarFormaObjeto(capsulaPrefab);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            CambiarFormaObjeto(cuboPrefab);
        }
    }

    void CambiarFormaObjeto(GameObject formaPrefab)
    {
        GameObject forma = Instantiate(formaPrefab, transform.position, transform.rotation);

        MeshFilter formaMeshFilter = forma.GetComponent<MeshFilter>();
        MeshRenderer formaMeshRenderer = forma.GetComponent<MeshRenderer>();
        MeshCollider formaMeshCollider = forma.GetComponent<MeshCollider>();

        MeshFilter objetoMeshFilter = GetComponent<MeshFilter>();
        MeshRenderer objetoMeshRenderer = GetComponent<MeshRenderer>();
        MeshCollider objetoMeshCollider = GetComponent<MeshCollider>();

        objetoMeshFilter.sharedMesh = formaMeshFilter.sharedMesh;

        formaMeshRenderer.sharedMaterial = jugadorMaterial;

        objetoMeshCollider.sharedMesh = formaMeshCollider.sharedMesh;

        Destroy(forma);

        if (formaPrefab == capsulaPrefab)
        {
            gameObject.tag = "Capsula";
        }
        else if (formaPrefab == cuboPrefab)
        {
            gameObject.tag = "Cubo";
        }
        else if (formaPrefab == esferaPrefab)
        {
            gameObject.tag = "Esfera";
        }
    }
}
