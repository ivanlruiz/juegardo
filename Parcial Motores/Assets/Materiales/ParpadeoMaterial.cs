using UnityEngine;

public class ParpadeoMaterial : MonoBehaviour
{
    public Material materialNormal;
    public Material materialParpadeo;
    public float tiempoParpadeo = 0.5f;
    public float fadeOutDuration = 0.2f;

    private Renderer cuboRenderer;

    private void Start()
    {
        cuboRenderer = GetComponent<Renderer>();
        // Iniciar el parpadeo
        StartCoroutine(Parpadear());
    }

    public void Parpadeo()
    {
        StartCoroutine(Parpadear());
    }

    private System.Collections.IEnumerator Parpadear()
    {
        // Cambiar al material de parpadeo
        cuboRenderer.material = materialParpadeo;
        yield return new WaitForSeconds(tiempoParpadeo);

        // Iniciar el fade out
        StartCoroutine(FadeOutMaterial());
    }

    private System.Collections.IEnumerator FadeOutMaterial()
    {
        // Obtener el color inicial y final del material
        Color initialColor = cuboRenderer.material.color;
        Color targetColor = new Color(initialColor.r, initialColor.g, initialColor.b, 0f);

        // Guardar el tiempo de inicio
        float startTime = Time.time;

        while (Time.time - startTime < fadeOutDuration)
        {
            // Calcular la cantidad de tiempo transcurrido desde el inicio
            float elapsedTime = Time.time - startTime;

            // Calcular la intensidad del color basado en el tiempo transcurrido
            float fadeAmount = Mathf.Lerp(1f, 0f, elapsedTime / fadeOutDuration);

            // Crear un nuevo color con la intensidad reducida
            Color newColor = new Color(initialColor.r, initialColor.g, initialColor.b, fadeAmount);

            // Asignar el nuevo color al material
            cuboRenderer.material.color = newColor;

            yield return null;
        }

        // Asignar el material normal una vez completado el fade out
        cuboRenderer.material = materialNormal;
    }
}
