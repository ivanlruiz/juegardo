using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour
{
    [SerializeField] public List<GameObject> _objectsToChange; // Lista de objetos en la escena
    public Material originalMaterial;
    public Material targetMaterial;
    public float fadeDuration = 1.0f;

    BeatController _beatcontroller;

    private bool isFading = false;

    private void Start()
    {
        _beatcontroller = FindObjectOfType<BeatController>();

        // Cambiar los materiales de los objetos a originalMaterial al inicio
        foreach (GameObject obj in _objectsToChange)
        {
            Renderer renderer = obj.GetComponent<Renderer>();
            renderer.material = originalMaterial;
        }
    }

    private void Update()
    {
        if (!isFading)
        {
            // Comprobar los momentos en los que se debe cambiar el material
            if (IsWithinRange(_beatcontroller._audioSource.time, 45.40f, 45.80f))
            {
                StartCoroutine(FadeOutMaterial(_objectsToChange[4]));
            }
            else if (IsWithinRange(_beatcontroller._audioSource.time, 45.80f, 46.20f))
            {
                StartCoroutine(FadeOutMaterial(_objectsToChange[2]));
            }
            else if (IsWithinRange(_beatcontroller._audioSource.time, 46.20f, 46.80f))
            {
                StartCoroutine(FadeOutMaterial(_objectsToChange[4]));
            }
            else if (_beatcontroller._audioSource.time >= 46.80f)
            {
                StartCoroutine(FadeOutMaterial(_objectsToChange[0]));
            }
        }
    }

    private IEnumerator FadeOutMaterial(GameObject obj)
    {
        isFading = true;

        Renderer renderer = obj.GetComponent<Renderer>();
        Material currentMaterial = renderer.material;

        float elapsedTime = 0f;
        while (elapsedTime < fadeDuration)
        {
            float t = elapsedTime / fadeDuration;
            Color fadedColor = Color.Lerp(currentMaterial.color, Color.black, t);
            renderer.material.color = fadedColor;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        renderer.material = targetMaterial;
        isFading = false;
    }

    private bool IsWithinRange(float value, float min, float max)
    {
        return value >= min && value < max;
    }
}
