using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BeatController : MonoBehaviour
{
    [SerializeField] private float _bpm;
    [SerializeField] public AudioSource _audioSource;
    [SerializeField] public Intervals[] _intervals;

    GeneradorObstaculos _obstaculos;
    Obstaculo _obstaculo;

    private TimeSpan _maxDuration = new TimeSpan(0, 5, 8); // Duraci�n m�xima de 5 minutos y 8 segundos
    private TimeSpan _currentDuration = TimeSpan.Zero; // Duraci�n actual

    private void Start()
    {
        _obstaculos = FindObjectOfType<GeneradorObstaculos>();
        _obstaculo = FindObjectOfType<Obstaculo>();

        StartCoroutine(CountDuration());
    }

    private void Update()
    {
        foreach (Intervals interval in _intervals)
        {
            float sampledTime = (_audioSource.timeSamples / (_audioSource.clip.frequency * interval.GetIntervalLength(_bpm)));
            interval.CheckForNewInterval(sampledTime);

            if (_obstaculos.generado == 12)
            {
                if (interval == _intervals[2])
                {
                    interval._steps = 0f;
                }
                if (interval == _intervals[0])
                {

                    interval._steps = 0.125f/2;
                }
                if (interval == _intervals[1])
                {
                    interval._steps = 0.125f / 2;
                }
            }

            if(_obstaculos.generado == 20)
            {
                if (interval == _intervals[2])
                {
                    interval._steps = 0f;
                }
                if (interval == _intervals[0])
                {

                    interval._steps = 0.25f;
                }
                if (interval == _intervals[1])
                {
                    interval._steps = 0.25f;
                }
            }
            
            
        }

        // Mostrar el tiempo de la canci�n en la consola
        //Debug.Log("Tiempo de la canci�n: " + FormatTimeSpan(_currentDuration));
    }

    private IEnumerator CountDuration()
    {
        while (_currentDuration < _maxDuration)
        {
            _currentDuration = _currentDuration.Add(TimeSpan.FromSeconds(Time.deltaTime));
            yield return null;
        }

        // Se ha alcanzado la duraci�n m�xima de 5 minutos y 8 segundos
        Debug.Log("Se ha alcanzado la duraci�n m�xima");
    }

    private string FormatTimeSpan(TimeSpan timeSpan)
    {
        return string.Format("{0}:{1:00}", (int)timeSpan.TotalSeconds, timeSpan.Milliseconds / 10, timeSpan.Milliseconds);
    }
}

[System.Serializable]
public class Intervals
{
    [SerializeField] public float _steps;
    [SerializeField] private UnityEvent _trigger;
    private int _lastInterval;

    public float _originalSteps;

    public float GetIntervalLength(float bpm)
    {
        return 60f / (bpm * _steps);
    }

    public void CheckForNewInterval(float interval)
    {
        if (Mathf.FloorToInt(interval) != _lastInterval)
        {
            _lastInterval = Mathf.FloorToInt(interval);
            _trigger.Invoke();
        }
    }
}
